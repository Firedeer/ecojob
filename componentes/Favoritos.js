import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Right,
  Left,
  Body
} from "native-base";
import TabOne from "./tabOne";
import TabTwo from "./tabTwo";
import TabThree from "./tabThree";
import styles from "./estilo_anatomia";
class Favoritos extends Component {
  render() {
    return (
      <Container >

        <Tabs  style={styles.hea}>
          <Tab heading="Servicios"  >
            <TabOne />
          </Tab>
          <Tab heading="Explorar">
            <TabTwo />
          </Tab>

        </Tabs>
      </Container>
    );
  }
}

export default Favoritos;
