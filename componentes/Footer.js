import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Footer,
  FooterTab,
  Text,
  Body,
  Left,
  Right,
  Icon
} from "native-base";
import styles from "./styles";
import Perfil from "./Perfil";
class IconText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab1: false,
      tab2: false,
      tab3: true,
      tab4: false,
      tab5:false
    };
  }
  toggleTab1() {
    this.setState({
      tab1: true,
      tab2: false,
      tab3: false,
      tab4: false,
      tab5:false
    });
  }
  toggleTab2() {
    this.setState({
      tab1: false,
      tab2: true,
      tab3: false,
      tab4: false,
      tab5:false
    });

  }
  toggleTab3() {
    this.setState({
      tab1: false,
      tab2: false,
      tab3: true,
      tab4: false,
      tab5:false
    });
  }
  toggleTab4() {
    this.setState({
      tab1: false,
      tab2: false,
      tab3: false,
      tab4: true,
      tab5:false
    });

  }
    toggleTab5() {
    this.setState({
      tab1: false,
      tab2: false,
      tab3: false,
      tab4: false,
      tab5:true
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        
        <Footer>
        <Content>
            {this.renderSelectedTab()}
          </Content>
          <FooterTab>
            <Button active={this.state.selectedTab==='profile'} 
               onPress={() => this.setState({selectedTab: 'profile'})} >
              <Icon name="home" />
              <Text style={styles.texto}>Explorar</Text>
            </Button>
            <Button active={this.state.tab2} onPress={() => this.toggleTab2()}>
              <Icon active={this.state.tab2} name="home" />
              <Text style={styles.texto}>Guardados</Text>
            </Button>
            <Button active={this.state.tab3} onPress={() => this.toggleTab3()}>
              <Icon active={this.state.tab3} name="home" />
              <Text style={styles.texto}>Servicios</Text>
            </Button>
            <Button active={this.state.tab4} onPress={() => this.toggleTab4()}>
              <Icon active={this.state.tab4} name="home" />
              <Text style={styles.texto}>Mensajes</Text>
            </Button>
              <Button active={this.state.tab5} onPress={() => this.toggleTab5()}>
              <Icon active={this.state.tab5} name="home" />
              <Text style={styles.texto}>Perfil.</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default IconText;
