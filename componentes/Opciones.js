import React, { Component } from 'react'; 
import {Platform,StyleSheet,Text,View,Dimensions} from 'react-native'; 
import { CardViewWithIcon,CardViewWithImage } from "react-native-simple-card-view"; 
import estilo from "./estilo_anatomia";
import Historial from "./Historial";
import Home from "./tabOne";
type Props = {}; 
const estilista=require('./imagenes/estilista.png');
const tecnicos = require('./imagenes/tecnicos3.png');
const automotriz = require('./imagenes/automotriz.png');
const construccion = require('./imagenes/construccion.png');
const eventos = require('./imagenes/eventos.png');
const acarreo = require('./imagenes/acarreo.png');
export default class tabOnde extends Component<Props> { 
  constructor(props) { 
    super(props); 
    this.state = ({ 
        github  : 0, 
        selectedTab:null,

       } 
     ) 
   } 
 


   render() { 

     return ( 
        <View style={ styles.container }> 
   
         <View style={ {alignItems:"center",flexDirection: "row",flexWrap:'wrap',} }> 
          <CardViewWithImage
        width={175}
        source={estilista}
        content={ 'Organiza desde donde quieres trabajar' }
        title={ 'Programar reserva' }
        imageWidth={50}
        imageHeight={ 50 }
        roundedImage={ false }
        onPress={() => this.props.navega("reserNormal")}

         />
        <CardViewWithImage
        width={175}
        source={construccion }
        content={ 'Deja que EcoJob escoja tu lugar de trabajo al azar' }
        title={ 'Reserva al azar' }
        imageWidth={50}
        imageHeight={ 50 }
        roundedImage={ false }
         onPress={() => this.props.navega("reserAzar")}
      />
      <View style={estilo.outerCircle}>
    
      <CardViewWithImage
        width={175}
        source={automotriz}
        content={ 'Escanea el lugar donde quieres Trabajar' }
        title={ 'Reserva instantanea' }
        imageWidth={50}
        imageHeight={ 50 }
        roundedImage={ false }
         onPress={ () => this.setState({selectedTab: 'Home'}) } 
      
      />
      </View>
         </View> 
       </View>
       
     ); 
   } 
 } 
 const styles = StyleSheet.create({container: {flex:2, alignItems:'center',backgroundColor:'#F5FCFF',paddingTop:20,},}); 
