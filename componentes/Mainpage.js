import React, { Component } from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Body,
  Left,
  Right
} from "native-base";

import styles from "./estilo_icon";

class BasicIcon extends Component {
  render() {
    return (
      <Container style={styles.container}>
  

        <Content padder>
          <View style={styles.iconContainer}>
            <Icon
              name="logo-apple"
              style={{ width: 45, height: 45, justifyContent: "center" }}
            />
            <Icon
              active
              name="pizza"
              style={{ width: 45, height: 45, justifyContent: "center" }}
            />
            <Icon
              name="person"
              style={{ width: 45, height: 45, justifyContent: "center" }}
            />
           
          </View>
        </Content>
      </Container>
    );
  }
}

export default BasicIcon;
