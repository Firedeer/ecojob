import React, {Component} from 'react';
import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';
import Anatomia from './Anatomia';
import reserNormal from './reservaNormal';
import reserAzar from './reservaAzar';
import { Root } from "native-base";
const AppNavegador = createStackNavigator({
  	Anatomia: { screen: Anatomia },
 	reserNormal: { screen: reserNormal },
 	reserAzar: { screen: reserAzar },
},
  {
    initialRouteName: 'Anatomia'
  }
);
const Navegador = createAppContainer(AppNavegador);


export default Navegador 
 