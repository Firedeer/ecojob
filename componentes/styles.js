const React = require("react-native");

const { StyleSheet } = React;

export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 10
  },
  texto: {
    fontSize:8.2,
    alignSelf: "center",
    color:'white'
  }
};
