import React, { Component } from 'react';
import { AppRegistry,StyleSheet,View,Picker, Text,Button } from 'react-native';
import { ViewPager } from 'rn-viewpager';

import StepIndicator from 'react-native-step-indicator';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import t from 'tcomb-form-native'; // 0.6.9
const PAGES = ['Page 1','Page 2','Page 3'];

const firstIndicatorStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize:40,
  separatorStrokeWidth: 3,
  currentStepStrokeWidth: 3,
  separatorFinishedColor: '#4aae4f',
  separatorUnFinishedColor: '#a4d4a5',
  stepIndicatorFinishedColor: '#4aae4f',
  stepIndicatorUnFinishedColor: '#a4d4a5',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 15,
  currentStepIndicatorLabelFontSize: 15,
  stepIndicatorLabelCurrentColor: '#000000',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
  labelColor: '#666666',
  labelSize: 12,
  currentStepLabelColor: '#4aae4f'
}


const estil = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
   row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 2
  },
  box: {
    flex: 1,
    height: 40,
    backgroundColor: '#333',
  },
  box2: {
    backgroundColor: 'green'
  },
  box3: {
    backgroundColor: 'orange',
      flex: 1.2
  },
  two: {
    flex: 1.2
  }
});

export default class reservaNormal extends Component {

  constructor() {
    super();
    this.state = {
      currentPage:0
    }
  }

  componentWillReceiveProps(nextProps,nextState) {
    if(nextState.currentPage != this.state.currentPage) {
      if(this.viewPager) {
        this.viewPager.setPage(nextState.currentPage)
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.stepIndicator}>
          <StepIndicator stepCount={3} customStyles={firstIndicatorStyles} currentPosition={this.state.currentPage} labels={["Account","Profile","Band"]} />
        </View>

        <ViewPager
          style={{flexGrow:1}}
          ref={(viewPager) => {this.viewPager = viewPager}}
          onPageSelected={(page) => {this.setState({currentPage:page.position})}}
          >

            {this.renderViewPagerPage(this.state.currentPage)}
          </ViewPager>
      </View>
    );
  }
//=======================
  renderViewPagerPage = (data) => {
  if(data==0)
  {
    return(
       <View style={estil.container}>
          <View style={estil.row}>
            <View style={[estil.box, estil.box3]}></View>
            <View style={[estil.box, estil.two]}></View>
            <View style={[estil.box, estil.box3]}></View>  
          </View>
          <View>
            <View style={[estil.box, estil.box3]}></View>
            <View style={[estil.box, estil.two]}></View>
            <View style={[estil.box, estil.box3]}></View>  
          </View>
          </View>
    )
}
//=======================
  else if(data==1)
  {
    return(<View style={styles.page}>
          <Button
  style={{fontSize: 20, color: 'green'}}
  styleDisabled={{color: 'red'}}
  onPress={() => {this.setState({currentPage:data+1})}}
  title="Siguiente"
>
</Button>
<Text>{this.state.currentPage}</Text>
    </View>)
}
//=======================
  else if(data==2)
  {
    return(<View style={styles.page}>
          <Button
  style={{fontSize: 20, color: 'green'}}
  styleDisabled={{color: 'red'}}
  onPress={() => {this.setState({currentPage:data+1})}}
  title="Siguiente"
>
</Button>
<Text>{this.state.currentPage}</Text>
    </View>)
}
else
{
      return(<View style={styles.page}>
          <Button
  style={{fontSize: 20, color: 'green'}}
  styleDisabled={{color: 'red'}}
  onPress={() => {this.setState({currentPage:data+1})}}
  title="Fin"
>
</Button>
<Text>{data}</Text>
    </View>)
}
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  stepIndicator: {
    marginVertical:50,
  },
  page: {
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
});