import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  View

} from "native-base";

import Bod from './Mainpage';
import styles from "./estilo_anatomia";
import stylesd from "./styles";
import Opciones from './Opciones';
import Favoritos from "./Favoritos";
import Historial from "./Historial";
import Mensajes from "./Mensajes";
import Perfil from "./Perfil";
import { StackNavigator } from 'react-navigation';
class Anatomy extends Component {
    constructor(props) {
    super(props);
    this.state = {
      tab1: false,
      tab2: false,
      tab3: true,
      tab4: false,
      tab5:false
    };
  }

    renderSelectedTab (navi) {

      if(this.state.selectedTab=="Home")
      {
        return (<Opciones navega={navi}/>);
      }
      else if(this.state.selectedTab=="Favoritos")
      {
         return (<Favoritos />);
      }
      else if(this.state.selectedTab=="Historial")
      {
         return (<Historial />);
      }
      else if(this.state.selectedTab=="Mensajes")
      {
         return (<Mensajes />);
      }
      else if(this.state.selectedTab=="Perfil")
      {
         return (<Perfil />);
      }
      else
      {
        return (<Opciones navega={navi}/>);
      }

  }
  render() {
 const {navigate} = this.props.navigation;
    return (
      <Container style={styles.container}>
      <View  style={styles.hea}></View>
      
       <Text style={styles.mensajeini}>EcoJob</Text>

        <Content style={styles.container}>
          {this.renderSelectedTab(navigate)}
        </Content>

        <Footer>
          <FooterTab>
             <Button active={this.state.selectedTab==='Home'} 
               onPress={() => this.setState({selectedTab: 'Home'})}
                >
             
              <Text style={styles.texto}>Reservar</Text>
            </Button>
              <Button active={this.state.selectedTab==='Favoritos'} 
               onPress={() => this.setState({selectedTab: 'Favoritos'})} >
           
              <Text style={styles.texto}>Mi reservas</Text>
            </Button>

            <Button active={this.state.selectedTab==='Mensajes'} 
               onPress={() => this.setState({selectedTab: 'Mensajes'})} >
           
              <Text style={styles.texto}>Mensajes</Text>
            </Button>
               <Button active={this.state.selectedTab==='Perfil'} 
               onPress={() => this.setState({selectedTab: 'Perfil'})} >
            
              <Text style={styles.texto}>Perfil.</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default Anatomy;
